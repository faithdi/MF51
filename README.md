% 假设x和y是已知的数组
y= xlsread('D:\tem\CCMF51-3000K-8.2kΩ阻温特性表.xlsx','Sheet1','D3:D33');
x= xlsread('D:\tem\CCMF51-3000K-8.2kΩ阻温特性表.xlsx','Sheet1','E3:E33');

% 定义非线性函数
fun = @(params, x) 2 * params(3) ./ (sqrt(params(2)^2 - 4 * params(3) * (params(1) - log(x))) - params(2)) - 273.15;

% 初始猜测的系数
params0 = [-2.3, 3768.39, -118235.54]; % 初始猜测值，根据实际情况调整

% 使用lsqcurvefit进行拟合
params_fit = lsqcurvefit(fun, params0, x, y);

% 提取拟合后的系数
a = params_fit(1);
b = params_fit(2);
c = params_fit(3);

% 输出结果
fprintf('系数 a = %f, b = %f, c = %f\n', a, b, c);